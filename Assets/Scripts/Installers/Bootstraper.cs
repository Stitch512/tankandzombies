﻿using System.Collections.Generic;
using Assets.Scripts.Entities;
using Assets.Scripts.Game.Entities.Factory;
using Assets.Scripts.Game.Managers;

namespace Assets.Scripts.Installers
{
	public class Bootstraper
	{
		private IEntityFactory _entityFactory;

		private IGameScene _scene;

		public Bootstraper(IGameScene scene, IEntityFactory entityFactory)
		{
			_scene = scene;
			_entityFactory = entityFactory;

			Initialize();
		}

		public void Initialize()
		{
			_scene.Initialize();

			return;

			List<IEntity> entities = new List<IEntity>();
			for (int i = 0; i < 100; i++)
			{
				IEntity entity = _entityFactory.Spawn("Unit_1");

				

				//_entityFactory.Despawn(entity);
			}

			//entities.ForEach(e => _entityFactory.Despawn(e));
		}
	}
}
