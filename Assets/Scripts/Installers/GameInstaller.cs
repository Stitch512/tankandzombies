﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Assets.Scripts.Common.Behaviour;
using Assets.Scripts.Common.Behaviour.Actions;
using Assets.Scripts.Entities;
using Assets.Scripts.Game.Entities;
using Assets.Scripts.Game.Entities.Factory;
using Assets.Scripts.Game.Entities.Weapon;
using Assets.Scripts.Game.Managers;
using Assets.Scripts.Game.UI;
using RTS_Cam;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Installers
{
	public class GameInstaller : MonoInstaller
	{
		//public EntitiesConfigs EntitiesConfig;

		[SerializeField]
		private GameScene _gameScene;

		[SerializeField]
		private CameraManager _camera;

		[SerializeField]
		private InputManager _inputManager;

		[SerializeField]
		private GameGUI _gui;

		public override void InstallBindings()
		{
			//Container.Bind<ITickable>().To<Entity>().AsTransient();

			InstallActions();

			Container.Bind<ICameraManager>().FromInstance(_camera).AsSingle();
			Container.Bind<IInputManager>().FromInstance(_inputManager).AsSingle();

			Container.Bind<IBattleParams>().To<BattleParams>().AsTransient();
			Container.Bind<IWeaponController>().To<WeaponController>().AsTransient();
			Container.Bind<IUnitWeapon>().To<UnitWeapon>().AsTransient();
			Container.Bind<IFactory<IUnitWeapon>>().To<UnitWeapon.Factory>().AsSingle();

			Container.Bind<IBehaviourManager>().To<BehaviourManager>().AsTransient();

			Container.Bind<IGameScene>().FromInstance(_gameScene).AsSingle();

			Container.Bind<IFactory<string, IEntity>>().To<CustomEntityFactory>().AsSingle();
			Container.Bind<IEntityFactory>().To<EntityPool>().AsSingle();

			GameSignalsInstaller.Install(Container);

			Container.Bind<Bootstraper>().AsSingle();

			Container.Bind<GameGUI>().FromComponentsInNewPrefab(_gui).AsSingle();

			Container.Resolve<GameGUI>();
			Container.Resolve<Bootstraper>();
		}

		private void InstallActions()
		{
			var classes = new List<Type>(Assembly.GetAssembly(typeof(GameAction))
				.GetTypes()
				.Where(t => t.IsSubclassOf(typeof(GameAction))).ToArray()).ToArray();
			foreach (var cClass in classes)
			{
				var attr = cClass.GetCustomAttribute<GameActionAttribute>();
				if (attr == null)
					continue;
				Container.BindInterfacesAndSelfTo(cClass).AsTransient();
			}
		}

		class EntityPool: EntityPoolBase<string, IEntity>, IEntityFactory
		{
			protected override string GetKey(IEntity item)
			{
				return item.Config.ConfigId;
			}

			protected override void OnSpawned(IEntity item, string id, IMemoryPool pool)
			{
				item.OnSpawned(id, pool);
			}

			protected override void OnDespawn(IEntity item)
			{
				item.OnDespawned();
			}
		}
	}
}
