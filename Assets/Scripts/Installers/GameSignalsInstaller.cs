﻿using Assets.Scripts.Game.Entities.Signals;
using Zenject;
using Zenject.SpaceFighter;

namespace Assets.Scripts.Installers
{
	/*public class PlayerDiedSignalObserver
	{
		public void OnPlayerDied()
		{
			Debug.Log("Fired PlayerDiedSignal");
		}
	}*/

	public class GameSignalsInstaller : Installer<GameSignalsInstaller>
	{
		public override void InstallBindings()
		{
			SignalBusInstaller.Install(Container);

			Container.DeclareSignal<UnitDeathSignal>();
			Container.DeclareSignal<UnitSpawnSignal>();
			Container.DeclareSignal<ReplaySignal>();
			Container.DeclareSignal<GameInitializeSignal>();

			//Container.BindSignal<PlayerDiedSignal>().ToMethod<PlayerDiedSignalObserver>(x => x.OnPlayerDied).FromNew();

			//Container.DeclareSignal<PlayerDiedSignal>();

			// Include these just to ensure BindSignal works
			//Container.BindSignal<PlayerDiedSignal>().ToMethod<PlayerDiedSignalObserver>(x => x.OnPlayerDied).FromNew();
			//Container.BindSignal<EnemyKilledSignal>().ToMethod(() => Debug.Log("Fired EnemyKilledSignal"));
		}
	}
}
