﻿using System.Collections.Generic;
using Assets.Scripts.Game.Configs;
using Assets.Scripts.Game.Configs.Data;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Installers
{
	[CreateAssetMenu(menuName = "Game/Test/Game Settings")]
	public class GameSettingsInstaller : ScriptableObjectInstaller<GameSettingsInstaller>
	{
		public EntitiesConfigs EntitiesConfig;

		public override void InstallBindings()
		{
			Container.Bind<IEntitiesConfigs>().To<EntitiesConfigs>().FromInstance(EntitiesConfig).AsSingle();

			//Container.BindInstance(EntitiesConfig).IfNotBound();
		}
	}
}
