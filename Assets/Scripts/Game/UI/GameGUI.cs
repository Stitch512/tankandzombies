﻿using Assets.Scripts.Game.Entities;
using Assets.Scripts.Game.Entities.Signals;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Assets.Scripts.Game.UI
{
	public class GameGUI : MonoBehaviour
	{
		[SerializeField] private Button _replayButton;

		[SerializeField] private GameObject _deathText;

		private SignalBus _signalBus;

		[Inject]
		private void Construct(SignalBus signalBus)
		{
			_signalBus = signalBus;

			_deathText.SetActive(false);

			_replayButton.onClick.AddListener(() =>
			{
				_signalBus.TryFire<ReplaySignal>();
			});

			_signalBus.Subscribe<UnitDeathSignal>(OnUnitDeath);
			_signalBus.Subscribe<GameInitializeSignal>(() =>
			{
				_deathText.SetActive(false);
			});
		}

		private void OnUnitDeath(UnitDeathSignal data)
		{
			if (data.Unit.Config.VisualConfig.Type == EntityType.Plaer)
			{
				_deathText.SetActive(true);
			}
		}
	}
}
