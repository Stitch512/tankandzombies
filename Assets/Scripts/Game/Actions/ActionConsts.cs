﻿namespace Assets.Scripts.Game.Actions
{
	public static class ActionConsts
	{
		//State names
		public static string Idle = "Idle";

		public static string Attack = "Attack";

		public static string Death = "Death";

		//Context var names
		public static string MoveRadius = "MoveRadius";

		public static string TargetPosition = "TargetPosition";

		public static string TargetUnit = "TargetUnit";

		public static string Speed = "Speed";

		public static string Damage = "Damage";

		public static string Marker = "Marker";

		public static string Wait = "Wait";
	}
}
