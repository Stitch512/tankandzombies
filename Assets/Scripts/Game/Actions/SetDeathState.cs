﻿using Assets.Scripts.Common.Behaviour.Actions;

namespace Assets.Scripts.Game.Actions
{
	[GameAction]
	public class SetDeathState : EntityAction
	{
		protected override void OnStart()
		{
			Context.Owner.Behaviour.Select(ActionConsts.Death);
			End(ActionStatus.Success);
		}
	}
}
