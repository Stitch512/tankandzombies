﻿using Assets.Scripts.Common.Behaviour.Actions;
using Assets.Scripts.Game.Entities;
using Assets.Scripts.Game.Entities.Signals;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Game.Actions
{
	[GameAction]
	public class DeathCheck: EntityAction
	{
		private IUnit _unit;

		protected override void OnStart()
		{
			_unit = Context.Owner as IUnit;
		}

		protected override void OnUpdate()
		{
			if (_unit.BattleParams.Health.Value <= 0)
			{
				Animator animator = _unit.Owner.GetComponentInChildren<Animator>();
				if (animator != null)
				{
					animator.SetTrigger("Death");
				}

				Context.SetFloat(ActionConsts.Wait, 1);

				End(ActionStatus.Success);
			}
		}

		protected override void OnEnd()
		{
			_unit = null;
		}
	}
}
