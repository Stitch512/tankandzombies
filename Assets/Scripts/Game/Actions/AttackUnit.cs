﻿using Assets.Scripts.Common.Behaviour.Actions;
using Assets.Scripts.Game.Entities;
using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.Game.Actions
{
	[GameAction]
	public class AttackUnit: EntityAction
	{
		private IUnit _owner;

		private float _attackLeftTime;

		protected override void OnStart()
		{
			_owner = Context.Owner as IUnit;
		}

		private bool TryAttack()
		{
			var targetObject = Context.GetGameObject(ActionConsts.TargetUnit);
			if (targetObject == null)
				return false;
			var unit = targetObject.Value.GetComponentInChildren<IUnit>();
			if (unit == null)
				return false;
			float distance = MathUtils.Distance2d(_owner.Position, unit.Position);
			if (!_owner.Weapon.SwitchWeapon(distance))
				return false;
			_owner.Weapon.CurrentWeapon.Apply(unit);
			_attackLeftTime = _owner.Weapon.CurrentWeapon.Config.AttackInterval;
			return true;
		}

		protected override void OnUpdate()
		{
			if (_attackLeftTime > 0)
			{
				_attackLeftTime -= Time.deltaTime;
				return;
			}

			if (!TryAttack())
			{
				End(ActionStatus.Success);
			}
		}

		protected override void OnEnd()
		{
			_owner = null;
		}
	}
}
