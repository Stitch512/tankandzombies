﻿using Assets.Scripts.Common.Behaviour.Actions;
using Assets.Scripts.Entities;
using UnityEngine;
using UnityEngine.AI;

namespace Assets.Scripts.Game.Actions
{
	[GameAction]
	public class MoveToPosition: EntityAction
	{
		private IEntity _owner;

		private NavMeshAgent _agent;

		protected override void OnStart()
		{
			_owner = Context.Owner;
			_agent = _owner.Owner.GetComponentInChildren<NavMeshAgent>();

			_owner.Position = SamplePosition(_owner.Position);
		}

		protected override void OnUpdate()
		{
			var targetPosition = Context.GetVector3(ActionConsts.TargetPosition);
			if (targetPosition == null)
			{
				End(ActionStatus.Failure);
				return;
			}

			Vector3 pos = targetPosition.Value;
			_agent.destination = SamplePosition(new Vector3(pos.x, _owner.Position.y, pos.z));

			if (IsComplete)
			{
				End(ActionStatus.Success);
			}
		}

		private bool IsComplete
		{
			get
			{
				var pos = _owner.Position;
				var destination = _agent.destination;
				float dx = pos.x - destination.x;
				float dz = pos.z - destination.z;
				return Mathf.Sqrt(dx * dx + dz * dz) < _agent.stoppingDistance;
			}
		}

		private Vector3 SamplePosition(Vector3 position)
		{
			NavMeshHit hit;
			if (NavMesh.SamplePosition(position, out hit, float.MaxValue, NavMesh.AllAreas))
			{
				return hit.position;
			}

			if (NavMesh.Raycast(new Vector3(position.x, 100, position.z),
				new Vector3(position.x, -100, position.z), out hit, NavMesh.AllAreas))
			{
				return hit.position;
			}

			return position;
		}

		protected override void OnEnd()
		{
			_owner = null;
			_agent = null;
		}
	}
}
