﻿using Assets.Scripts.Common.Behaviour.Actions;

namespace Assets.Scripts.Game.Actions
{
	[GameAction]
	public class DestroyEntity : EntityAction
	{
		protected override void OnStart()
		{
			Context.Owner.Destroy();
			End(ActionStatus.Success);
		}
	}
}
