﻿using Assets.Scripts.Common.Behaviour.Actions;

namespace Assets.Scripts.Game.Actions
{
	[GameAction]
	public class SetAttachState: EntityAction
	{
		protected override void OnStart()
		{
			Context.Owner.Behaviour.Select(ActionConsts.Attack);
			End(ActionStatus.Success);
		}
	}
}
