﻿using Assets.Scripts.Common.Behaviour.Actions;
using Assets.Scripts.Entities;
using Assets.Scripts.Game.Configs;
using Assets.Scripts.Game.Entities;
using Assets.Scripts.Utils;
using UnityEngine;
using UnityEngine.AI;

namespace Assets.Scripts.Game.Actions
{
	[GameAction]
	public class MoveToAttack : EntityAction
	{
		private IEntity _owner;

		private NavMeshAgent _agent;

		private Vector2 _attackRange;

		private float _agroRange;

		protected override void OnStart()
		{
			_owner = Context.Owner;
			_agent = _owner.Owner.GetComponentInChildren<NavMeshAgent>();

			IUnit unit = _owner as IUnit;
			_attackRange = unit.Weapon.AttackRange;

			var buttleConfig = Context.Owner.GetConfig<IUnitConfig>().BattleConfig;
			_agroRange = buttleConfig.AgroRange;
			_agent.speed = buttleConfig.Speed;

			_owner.Position = SamplePosition(_owner.Position);
		}

		protected override void OnUpdate()
		{
			var targetObject = Context.GetGameObject(ActionConsts.TargetUnit);
			if (targetObject == null)
			{
				End(ActionStatus.Failure);
				return;
			}

			Vector3 targetPosition;
			if (!TryMoveToTarget(targetObject.Value.transform, out targetPosition))
			{
				End(ActionStatus.Success);
				return;
			}

			_agent.destination = SamplePosition(new Vector3(targetPosition.x, _owner.Position.y, targetPosition.z));

			if (IsComplete)
			{
				End(ActionStatus.Success);
			}
		}

		private bool TryMoveToTarget(Transform target, out Vector3 targetPosition)
		{
			targetPosition = Vector3.positiveInfinity;

			if (target == null)
				return false;

			float stopDistance = 0.000f;

			Vector3 direction = target.position - _owner.Position;
			float distance = direction.magnitude;

			if (distance > _agroRange)
			{
				return false;
			}

			if (distance > _attackRange.y + stopDistance)
			{
				//targetPosition = target.position + direction.normalized * (distance - (_attackRange.y + stopDistance));
				targetPosition = target.position + direction.normalized * _attackRange.y;
				return true;
			}
			else if (distance < _attackRange.x - stopDistance)
			{
				//targetPosition = target.position - direction.normalized * ((_attackRange.x - stopDistance) - distance);
				targetPosition = target.position - direction.normalized * _attackRange.x;
				return true;
			}

			return false;
		}

		private bool IsComplete
		{
			get
			{
				return _owner.IsDestinated(_agent.destination, _agent.stoppingDistance);
				/*var pos = _owner.Position;
				var destination = _agent.destination;
				float dx = pos.x - destination.x;
				float dz = pos.z - destination.z;
				return Mathf.Sqrt(dx * dx + dz * dz) < _agent.stoppingDistance;*/
			}
		}

		private Vector3 SamplePosition(Vector3 position)
		{
			NavMeshHit hit;
			if (NavMesh.SamplePosition(position, out hit, float.MaxValue, NavMesh.AllAreas))
			{
				return hit.position;
			}

			if (NavMesh.Raycast(new Vector3(position.x, 100, position.z),
				new Vector3(position.x, -100, position.z), out hit, NavMesh.AllAreas))
			{
				return hit.position;
			}

			return position;
		}

		protected override void OnEnd()
		{
			_owner = null;
			_agent = null;
		}
	}
}
