﻿using Assets.Scripts.Common.Behaviour.Actions;
using UnityEngine;

namespace Assets.Scripts.Game.Actions
{
	[GameAction]
	public class SetRandomTarget: EntityAction
	{
		protected override void OnStart()
		{
			var owner = Context.Owner;
			var radius = Context.GetFloat(ActionConsts.MoveRadius);
			var ownerPos = owner.Position;
			var randomPos = Random.insideUnitCircle * radius.Value;
			Context.SetVector3(ActionConsts.TargetPosition, new Vector3(ownerPos.x + randomPos.x, randomPos.y, ownerPos.z + randomPos.y));
			End(ActionStatus.Success);
		}
	}
}
