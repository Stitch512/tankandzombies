﻿using Assets.Scripts.Common.Behaviour.Actions;
using Assets.Scripts.Game.Entities;
using Assets.Scripts.Game.Managers;
using UnityEngine;

namespace Assets.Scripts.Game.Actions
{
	[GameAction]
	public class ChangePlayerWeapon: EntityAction
	{
		private IInputManager _inputManager;

		private IUnit _unit;

		private bool _isAttack;

		private float _attackLeftTime;

		public ChangePlayerWeapon(IInputManager inputManager)
		{
			_inputManager = inputManager;
		}

		protected override void OnStart()
		{
			_unit = Context.Owner as IUnit;
			_inputManager.KeyDown += OnKeyDown;
			_inputManager.KeyUp += OnKeyUp;
		}

		private void OnKeyDown(KeyCode key)
		{
			switch (key)
			{
				case KeyCode.W:
					_unit.Weapon.NextWeapon();
					break;
				case KeyCode.Q:
					_unit.Weapon.PrevWeapon();
					break;
				case KeyCode.X:
					if (_unit.Weapon.CurrentWeapon != null)
					{
						_unit.Weapon.CurrentWeapon?.Apply();
						_isAttack = true;
						_attackLeftTime = _unit.Weapon.CurrentWeapon.Config.AttackInterval;
					}
					break;
			}
		}

		private void OnKeyUp(KeyCode key)
		{
			switch (key)
			{
				case KeyCode.X:
					_isAttack = false;
					break;
			}
		}

		protected override void OnUpdate()
		{
			if (_isAttack && _unit.Weapon.CurrentWeapon != null)
			{
				if (_attackLeftTime < 0)
				{
					_unit.Weapon.CurrentWeapon.Apply();
					_attackLeftTime = _unit.Weapon.CurrentWeapon.Config.AttackInterval;
				}
				else
				{
					_attackLeftTime -= Time.deltaTime;
				}
			}
		}

		protected override void OnEnd()
		{
			_unit = null;
			_inputManager.KeyDown -= OnKeyDown;
			_inputManager.KeyUp -= OnKeyUp;
		}
	}
}
