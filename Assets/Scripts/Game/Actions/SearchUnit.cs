﻿using Assets.Scripts.Common.Behaviour.Actions;
using Assets.Scripts.Entities;
using Assets.Scripts.Game.Configs;
using Assets.Scripts.Game.Entities;
using UnityEngine;

namespace Assets.Scripts.Game.Actions
{
	[GameAction]
	public class SearchUnit: EntityAction
	{
		private IEntity _entity;

		private float _argoRange;

		private int _searchLayer = 1 << LayerMask.NameToLayer("Player");

		protected override void OnStart()
		{
			_entity = Context.Owner;

			if (Context.GetGameObject(ActionConsts.TargetUnit) != null)
			{
				End(ActionStatus.Success);
				return;
			}

			_argoRange = _entity.GetConfig<IUnitConfig>().BattleConfig.AgroRange;
		}

		protected override void OnUpdate()
		{
			var colliders = Physics.OverlapSphere(_entity.Position, _argoRange, _searchLayer);
			foreach (var collider in colliders)
			{
				IUnit unit = collider.GetComponentInParent<IUnit>();
				if (unit == null || unit == _entity)
					continue;

				Context.SetGameObject(ActionConsts.TargetUnit, unit.Owner);
				End(ActionStatus.Success);
				break;
			}
		}

		protected override void OnEnd()
		{
			_entity = null;
			_argoRange = 0;
		}
}
}
