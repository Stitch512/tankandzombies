﻿using Assets.Scripts.Common.Behaviour.Actions;
using UnityEngine;

namespace Assets.Scripts.Game.Actions
{
	[GameAction]
	public class Wait: EntityAction
	{
		private float _leftTime;

		protected override void OnStart()
		{
			_leftTime = Context.GetFloat(ActionConsts.Wait);
		}

		protected override void OnUpdate()
		{
			if (_leftTime < 0)
			{
				End(ActionStatus.Success);
				return;
			}

			_leftTime -= Time.deltaTime;
		}

		protected override void OnEnd()
		{
			base.OnEnd();
		}
	}
}
