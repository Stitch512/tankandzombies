﻿using Assets.Scripts.Common.Behaviour.Actions;

namespace Assets.Scripts.Game.Actions
{
	[GameAction]
	public class SetIdleState : EntityAction
	{
		protected override void OnStart()
		{
			Context.Owner.Behaviour.Select(ActionConsts.Idle);
			End(ActionStatus.Success);
		}
	}
}
