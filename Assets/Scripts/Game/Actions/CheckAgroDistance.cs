﻿using Assets.Scripts.Common.Behaviour.Actions;
using Assets.Scripts.Game.Configs;
using Assets.Scripts.Utils;

namespace Assets.Scripts.Game.Actions
{
	[GameAction]
	public class CheckAgroDistance: EntityAction
	{
		protected override void OnStart()
		{
			float agroRange = Context.Owner.GetConfig<IUnitConfig>().BattleConfig.AgroRange;

			var target = Context.GetGameObject(ActionConsts.TargetUnit);
			if (target == null)
			{
				End(ActionStatus.Failure);
				return;
			}

			if (!Context.Owner.IsDestinated(target.Value.transform.position, agroRange))
			{
				Context.RemoveGameObject(ActionConsts.TargetUnit);
				End(ActionStatus.Failure);
				return;
			}

			End(ActionStatus.Success);
		}
	}
}
