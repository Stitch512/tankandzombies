﻿using Assets.Scripts.Common.Behaviour.Actions;

namespace Assets.Scripts.Game.Actions
{
	[GameAction]
	public class InitializeEnemy: EntityAction
	{
		protected override void OnStart()
		{
			Context.SetFloat(ActionConsts.MoveRadius, 5);
			End(ActionStatus.Success);
		}
	}
}
