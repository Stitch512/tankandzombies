﻿using Assets.Scripts.Common.Behaviour.Actions;

namespace Assets.Scripts.Game.Actions
{
	[GameAction]
	public class TankDrive: EntityAction
	{
		private WheelDrive _drive;

		protected override void OnStart()
		{
			_drive = Context.Owner.Owner.GetComponent<WheelDrive>();
		}

		protected override void OnUpdate()
		{
			if (_drive != null)
			{
				_drive.OnUpdate();
			}
		}

		protected override void OnEnd()
		{
			_drive = null;
		}
	}
}
