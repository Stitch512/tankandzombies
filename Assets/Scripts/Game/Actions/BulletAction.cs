﻿using Assets.Scripts.Common.Behaviour.Actions;
using Assets.Scripts.Entities;
using Assets.Scripts.Game.Entities;
using UnityEngine;

namespace Assets.Scripts.Game.Actions
{
	[GameAction]
	public class BulletAction: EntityAction
	{
		private IEntity _owner;

		private float _speed = 3;

		private float _damage = 3;

		private float _leftTime;

		private int _searchLayer = 1 << LayerMask.NameToLayer("Enemy");

		protected override void OnStart()
		{
			var marker = Context.GetGameObject(ActionConsts.Marker).Value.transform;
			_speed = Context.GetFloat(ActionConsts.Speed);
			_damage = Context.GetFloat(ActionConsts.Damage);
			_owner = Context.Owner;

			//_owner.CollisionEnter += OnCollisionEnter;

			_leftTime = 10f;
		}

		private void OnCollisionEnter(Collision collision)
		{
			IUnit unit = collision.transform.GetComponentInParent<IUnit>();
			if (unit == null || unit.Owner.tag != "Enemy")
				return;
			unit.BattleParams.Health.Value -= _damage * (1 - unit.BattleParams.Armor);
			End(ActionStatus.Success);
		}

		protected override void OnUpdate()
		{
			if (_leftTime < 0)
			{
				End(ActionStatus.Success);
				return;
			}

			_leftTime -= Time.deltaTime;
			_owner.Position += _owner.Rotation * Vector3.forward * Time.deltaTime * _speed;

			var colliders = Physics.OverlapSphere(_owner.Position, 0.1f, _searchLayer);
			foreach (var collider in colliders)
			{
				IUnit unit = collider.GetComponentInParent<IUnit>();
				if (unit == null)
					continue;

				unit.BattleParams.Health.Value -= _damage * (1 - unit.BattleParams.Armor);
				End(ActionStatus.Success);
				break;
			}
		}

		protected override void OnEnd()
		{
			//_owner.CollisionEnter += OnCollisionEnter;

			_owner.Destroy();

			_owner = null;
		}
	}
}
