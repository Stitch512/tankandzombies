﻿namespace Assets.Scripts.Game.Configs
{
	public interface IWeaponConfig
	{
		string WeaponId { get; }

		float AttackRangeMin { get; }

		float AttackRangeMax { get; }

		float AttackInterval { get; }

		float Damage { get; }

		float Speed { get; }

		string PrefabId { get; }
	}
}
