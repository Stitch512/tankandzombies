﻿using System.Collections.Generic;

namespace Assets.Scripts.Game.Configs
{
	public interface IBattleConfig
	{
		float AgroRange { get; }

		float Health { get; }

		float Armor { get; }

		float Speed { get; }

		List<IWeaponConfig> Weapons { get; }
	}
}
