﻿using System.Collections.Generic;
using Assets.Scripts.Common.Behaviour.Config;

namespace Assets.Scripts.Game.Configs
{
	public interface IEntityConfig
	{
		string ConfigId { get; }

		IVisualConfig VisualConfig { get; }

		List<BehaviourConfig> Behaviours { get; }
	}
}
