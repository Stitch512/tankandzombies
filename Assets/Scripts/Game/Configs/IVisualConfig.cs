﻿using System.Collections.Generic;
using Assets.Scripts.Entities.Visual;
using Assets.Scripts.Game.Entities;

namespace Assets.Scripts.Game.Configs
{
	public interface IVisualConfig
	{
		EntityType Type { get; }

		List<EntityVisual> VisualsPrefabs { get; }

		Entity RootPrefab { get; }
	}
}
