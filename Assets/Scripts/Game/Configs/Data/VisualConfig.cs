﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Common.Config;
using Assets.Scripts.Entities.Visual;
using Assets.Scripts.Game.Entities;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Game.Configs.Data
{
	[Serializable]
	public class VisualConfig: IVisualConfig, IGUIDrawable
	{
		[SerializeField]
		private EntityType _type;

		[SerializeField]
		private List<EntityVisual> _visualPrefabsPrefabs = new List<EntityVisual>();

		[SerializeField]
		private Entity _rootPrefabPrefab;

		public EntityType Type => _type;

		public List<EntityVisual> VisualsPrefabs => _visualPrefabsPrefabs;

		public Entity RootPrefab => _rootPrefabPrefab;

#if UNITY_EDITOR
		public void Draw()
		{
			_type = (EntityType)EditorGUILayout.EnumPopup("Type", _type);
			//_visualPrefab = EditorGUILayout.ObjectField("Visual", _visualPrefab, typeof(EntityVisual), false) as EntityVisual;

			GUIUtils.DrawObjectsList("VisualsPrefabs", _visualPrefabsPrefabs);

			_rootPrefabPrefab = EditorGUILayout.ObjectField("RootPrefab", _rootPrefabPrefab, typeof(Entity), false) as Entity;
		}
#endif
	}
}
