﻿using System;
using Assets.Scripts.Common.Config;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Game.Configs.Data
{
	[Serializable]
	public class WeaponConfig: IWeaponConfig, IGUIDrawable
	{
		[SerializeField]
		private string _weaponId;

		[SerializeField]
		private float _attackRangeMin;

		[SerializeField]
		private float _attackRangeMax;

		[SerializeField]
		private float _attackInterval;

		[SerializeField]
		private float _damage;

		[SerializeField]
		private float _speed;

		[SerializeField]
		private string _prefabId;

		public string WeaponId => _weaponId;

		public float AttackRangeMin => _attackRangeMin;

		public float AttackRangeMax => _attackRangeMax;

		public float AttackInterval => _attackInterval;

		public float Damage => _damage;

		public float Speed => _speed;

		public string PrefabId => _prefabId;


#if UNITY_EDITOR
		public void Draw()
		{
			EditorGUILayout.BeginVertical();

			GUILayout.Space(10);

			_weaponId = EditorGUILayout.TextField("WeaponId", _weaponId);
			_attackRangeMin = EditorGUILayout.FloatField("AttackRangeMin", _attackRangeMin);
			_attackRangeMax = EditorGUILayout.FloatField("AttackRangeMax", _attackRangeMax);
			_attackInterval = EditorGUILayout.FloatField("AttackInterval", _attackInterval);
			_damage = EditorGUILayout.FloatField("Damage", _damage);
			_speed = EditorGUILayout.FloatField("Speed", _speed);
			_prefabId = EditorGUILayout.TextField("PrefabId", _prefabId);

			GUILayout.Space(10);

			EditorGUILayout.EndVertical();
		}
#endif
	}
}
