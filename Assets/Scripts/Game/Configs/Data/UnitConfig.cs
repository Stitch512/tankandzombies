﻿using System;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Game.Configs.Data
{
	[CreateAssetMenu(fileName = "UnitConfig", menuName = "Game/UnitConfig", order = 1)]
	[Serializable]
	public class UnitConfig: EntityConfig, IUnitConfig
	{
		[SerializeField]
		private BattleConfig _battleConfig = new BattleConfig();

		public IBattleConfig BattleConfig => _battleConfig;

#if UNITY_EDITOR
		protected override void OnDraw()
		{
			base.OnDraw();

			_battleConfig.Draw();
			EditorGUILayout.Separator();
		}
#endif
	}
}
