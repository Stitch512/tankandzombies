﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Common.Config;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Game.Configs.Data
{
	[Serializable]
	public class BattleConfig: IBattleConfig, IGUIDrawable
	{
		[SerializeField]
		private float _agroRange;

		[SerializeField]
		private float _health;

		[SerializeField]
		private float _armor;

		[SerializeField]
		private float _speed;

		[SerializeField]
		private List<WeaponConfig> _weapons = new List<WeaponConfig>();

		public float AgroRange => _agroRange;

		public float Health => _health;

		public float Armor => _armor;

		public float Speed => _speed;

		public List<IWeaponConfig> Weapons => _weapons.Cast<IWeaponConfig>().ToList();

#if UNITY_EDITOR
		Vector2 scrolPos;
		public void Draw()
		{
			scrolPos = EditorGUILayout.BeginScrollView(scrolPos);

			_agroRange = EditorGUILayout.FloatField("AgroRange", _agroRange);
			_health = EditorGUILayout.FloatField("Health", _health);
			_armor = EditorGUILayout.FloatField("Armor", _armor);
			_speed = EditorGUILayout.FloatField("Speed", _speed);

			GUIUtils.DrawList("Weapons", _weapons);

			EditorGUILayout.EndScrollView();
		}
#endif

	}
}
