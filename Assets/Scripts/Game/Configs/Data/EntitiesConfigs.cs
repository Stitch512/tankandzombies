﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Common.Config;
using Assets.Scripts.Game.Entities;
using UnityEngine;

namespace Assets.Scripts.Game.Configs.Data
{
	[CreateAssetMenu(fileName = "EntityConfigsList", menuName = "Game/EntityConfigsList", order = 1)]
	[Serializable]
	public class EntitiesConfigs: ConfigsList<EntityConfig>, IEntitiesConfigs
	{
		[SerializeField]
		private Entity _defaultRootPrefab;

		public Entity DefaultRootPrefab => _defaultRootPrefab;

		public List<IEntityConfig> Configs => this.Cast<IEntityConfig>().ToList();

		public IEntityConfig GetConfig(string id)
		{
			return _childs.FirstOrDefault(c => c.ConfigId == id);
		}

		public IEntityConfig GetConfig(EntityType type)
		{
			return _childs.FirstOrDefault(c => c.VisualConfig?.Type == type);
		}

		
	}
}
