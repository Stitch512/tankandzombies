﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Common.Behaviour.Config;
using Assets.Scripts.Common.Config;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Game.Configs.Data
{
	[CreateAssetMenu(fileName = "EntityConfig", menuName = "Game/EntityConfig", order = 1)]
	[Serializable]
	public class EntityConfig: ScriptableObjectConfig, IEntityConfig
	{
		[SerializeField]
		private VisualConfig _visualConfig = new VisualConfig();

		public IVisualConfig VisualConfig => _visualConfig;

		[SerializeField]
		private List<BehaviourConfig> _behaviours = new List<BehaviourConfig>();

		public List<BehaviourConfig> Behaviours => _behaviours;

#if UNITY_EDITOR
		protected override void OnDraw()
		{
			base.OnDraw();

			ConfigId = EditorGUILayout.DelayedTextField("ID", ConfigId);

			EditorGUILayout.Separator();

			_visualConfig.Draw();
			EditorGUILayout.Separator();

			GUIUtils.DrawScriptableObjectsList("Behaviours", _behaviours);
			EditorGUILayout.Separator();
		}
#endif
	}
}
