﻿using System.Collections.Generic;

namespace Assets.Scripts.Game.Configs
{
	public interface IEntitiesConfigs 
	{
		//Entity DefaultRootPrefab { get; }

		List<IEntityConfig> Configs { get; }

		IEntityConfig GetConfig(string id);

		//IEntityConfig GetConfig(EntityType type);
	}
}
