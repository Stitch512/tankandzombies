﻿namespace Assets.Scripts.Game.Configs
{
	public interface IUnitConfig: IEntityConfig
	{
		IBattleConfig BattleConfig { get; }
	}
}
