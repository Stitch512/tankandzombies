﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Game.Managers
{

	public class InputManager : MonoBehaviour, IInputManager
	{
		[SerializeField]
		private Camera _camera;

		public event Action<RaycastHit> Click;

		public event Action<Vector3> MouseDown;

		public event Action<KeyCode> KeyDown;

		public event Action<KeyCode> KeyUp;

		protected List<KeyCode> m_activeInputs = new List<KeyCode>();

		private Array _keys = System.Enum.GetValues(typeof(KeyCode));

		private void Update_()
		{
			if (IsOverGUI())
				return;

			if (Input.GetMouseButtonDown(0))
			{
				MouseDown?.Invoke(Input.mousePosition);

				Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
				RaycastHit hit;
				if (Physics.Raycast(ray, out hit))
				{
					Click?.Invoke(hit);
				}
			}

			Event e = Event.current;
			if (e?.type == EventType.KeyDown)
			{
				if (Input.GetKeyDown(e.keyCode))
				{
					KeyDown?.Invoke(e.keyCode);
				}
			}
			else if (e?.type == EventType.KeyUp)
			{
				if (Input.GetKeyUp(e.keyCode))
				{
					KeyUp?.Invoke(e.keyCode);
				}
			}
		}

		public void Update()
		{
			if (IsOverGUI())
				return;

			if (Input.GetMouseButtonDown(0))
			{
				MouseDown?.Invoke(Input.mousePosition);

				Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
				RaycastHit hit;
				if (Physics.Raycast(ray, out hit))
				{
					Click?.Invoke(hit);
				}
			}

			foreach (KeyCode code in _keys)
			{
				if (Input.GetKeyDown(code))
				{
					KeyDown?.Invoke(code);
				}
				if (Input.GetKeyUp(code))
				{
					KeyUp?.Invoke(code);
				}
			}

			/*
			List<KeyCode> pressedInput = new List<KeyCode>();

			if (Input.anyKeyDown || Input.anyKey)
			{
				foreach (KeyCode code in System.Enum.GetValues(typeof(KeyCode)))
				{
					if (Input.GetKeyDown(code))
					{
						m_activeInputs.Remove(code);
						m_activeInputs.Add(code);
						pressedInput.Add(code);

						KeyDown?.Invoke(code);
					}
				}
			}

			List<KeyCode> releasedInput = new List<KeyCode>();

			foreach (KeyCode code in m_activeInputs)
			{
				releasedInput.Add(code);

				if (!pressedInput.Contains(code))
				{
					releasedInput.Remove(code);

					KeyUp?.Invoke(code);
				}
			}

			m_activeInputs = releasedInput;*/
		}


		private bool IsOverGUI()
		{
			if (UnityEngine.EventSystems.EventSystem.current == null)
				return false;
			if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
				return true;

			if (Input.touchCount > 0 && Input.GetTouch(0).phase != TouchPhase.Ended)
			{
				if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
					return true;
			}

			return false;
		}
	}
}
