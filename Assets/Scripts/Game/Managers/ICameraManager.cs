﻿using UnityEngine;

namespace Assets.Scripts.Game.Managers
{
	public interface ICameraManager
	{
		void SetTarget(Transform target);
	}
}
