﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RTS_Cam;
using UnityEngine;

namespace Assets.Scripts.Game.Managers
{
	public class CameraManager: MonoBehaviour, ICameraManager
	{
		public void SetTarget(Transform target)
		{
			var rtsCamera =  GetComponent<RTS_Camera>();
			if (rtsCamera == null)
				return;
			rtsCamera.SetTarget(target);
		}
	}
}
