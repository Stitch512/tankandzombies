﻿using System;
using UnityEngine;

namespace Assets.Scripts.Game.Managers
{
	public interface IInputManager
	{
		event Action<RaycastHit> Click;

		event Action<Vector3> MouseDown;

		event Action<KeyCode> KeyDown;

		event Action<KeyCode> KeyUp;

	}

}
