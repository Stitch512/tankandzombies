﻿using System.Collections.Generic;
using Assets.Scripts.Game.Actions;
using Assets.Scripts.Game.Configs.Data;
using Assets.Scripts.Game.Entities;
using Assets.Scripts.Game.Entities.Factory;
using Assets.Scripts.Game.Entities.Signals;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Game.Managers
{
	public class GameScene: MonoBehaviour, IGameScene
	{
		[SerializeField]
		private UnitConfig _playerConfig;

		[SerializeField]
		private List<UnitConfig> _enemiesConfigs = new List<UnitConfig>();

		[SerializeField]
		private List<Transform> _spawnPoints = new List<Transform>();

		[SerializeField]
		private int _maxUnitsCount;

		private IEntityFactory _entityFactory;

		private ICameraManager _camera;

		private IUnit _player;

		private List<IUnit> _enemies = new List<IUnit>();

		private SignalBus _signalBus;

		[Inject]
		private void Construct(SignalBus signalBus, IEntityFactory entityFactory, ICameraManager camera)
		{
			_signalBus = signalBus;
			_entityFactory = entityFactory;
			_camera = camera;
		}

		private void OnUnitSpawn(UnitSpawnSignal data)
		{
			switch (data.Unit.Config.VisualConfig.Type)
			{
				case EntityType.Plaer:
					_player = data.Unit;
					break;
				case EntityType.Enemy:
					_enemies.Add(data.Unit);
					break;
			}
		}

		private void OnUnitDeath(UnitDeathSignal data)
		{
			switch (data.Unit.Config.VisualConfig.Type)
			{
				case EntityType.Plaer:
					_player = null;
					break;
				case EntityType.Enemy:
					_enemies.Remove(data.Unit);
					break;
			}

			SpawnUnits();
		}

		private void SpawnUnits()
		{
			if (_spawnPoints.Count == 0 || _enemiesConfigs.Count == 0)
				return;
			if (_enemies.Count >= _maxUnitsCount)
				return;
			int count = _maxUnitsCount - _enemies.Count;
			for (int i = 0; i < count; i++)
			{
				var spawnTarget = _spawnPoints[Random.Range(0, _spawnPoints.Count)];
				var config = _enemiesConfigs[Random.Range(0, _enemiesConfigs.Count)];
				var unit = _entityFactory.Spawn(config.ConfigId);
				unit.Position = spawnTarget.position;
				unit.Behaviour.Select(ActionConsts.Idle);
			}
		}

		public void Initialize()
		{
			Clear();

			_player = _entityFactory.Spawn(_playerConfig.ConfigId) as IUnit;
			_player.Position = Vector3.zero;
			_player.Behaviour.Select(ActionConsts.Idle);

			_camera.SetTarget(_player.Owner.transform);

			_signalBus.Subscribe<UnitSpawnSignal>(OnUnitSpawn);
			_signalBus.Subscribe<UnitDeathSignal>(OnUnitDeath);
			_signalBus.Subscribe<ReplaySignal>(Initialize);

			SpawnUnits();

			_signalBus.TryFire<GameInitializeSignal>();

			/*var spawners = FindObjectsOfType<EnemySpawner>();
			foreach (var spawner in spawners)
			{
				Vector3 spawnPos = spawner.transform.position;
				var entity = _entityFactory.Spawn(spawner.UnitId);
				entity.Position = spawnPos;
				entity.Behaviour.Select("Idle");
				//entity.Destroy();

				//entity.

				if (spawner.UnitId == "Player")
				{
					_camera.SetTarget(entity.Owner.transform);
				}
			}*/
		}

		public void Clear()
		{
			_signalBus.TryUnsubscribe<UnitSpawnSignal>(OnUnitSpawn);
			_signalBus.TryUnsubscribe<UnitDeathSignal>(OnUnitDeath);
			_signalBus.TryUnsubscribe<ReplaySignal>(Initialize);

			_player?.Destroy();
			_player = null;

			_camera.SetTarget(null);

			foreach (var item in _enemies)
			{
				item.Destroy();
			}
			_enemies.Clear();
		}
	}
}
