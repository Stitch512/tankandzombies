﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Game.Configs;
using UnityEngine;

namespace Assets.Scripts.Game.Entities.Weapon
{
	public interface IWeaponController
	{
		event Action<IUnitWeapon> Change;

		Vector2 AttackRange { get; }

		List<IUnitWeapon> Weapons { get; }

		void SetConfig(List<IWeaponConfig> configs);

		IUnitWeapon CurrentWeapon { get; }

		bool SwitchWeapon(float distance);

		void NextWeapon();

		void PrevWeapon();
	}
}
