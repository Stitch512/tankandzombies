﻿using Assets.Scripts.Entities;
using Assets.Scripts.Game.Actions;
using Assets.Scripts.Game.Configs;
using Assets.Scripts.Game.Entities.Factory;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Game.Entities.Weapon
{
	public class UnitWeapon: IUnitWeapon
	{
		private readonly IEntityFactory _entityFactory;

		public IWeaponConfig Config { get; private set;  }

		private Transform _marker;

		public Transform Marker => _marker;

		public UnitWeapon(IEntityFactory entityFactory)
		{
			_entityFactory = entityFactory;
		}

		public void SetConfig(IWeaponConfig config)
		{
			Config = config;
		}

		public void SetMarker(Transform marker)
		{
			_marker = marker;
		}

		public void Apply()
		{
			if(_marker == null)
				return;

			IEntity bullet = CreateBullet();
			if (bullet == null)
				return;
			bullet.Behaviour.Context.SetGameObject(ActionConsts.Marker, _marker.gameObject);
			bullet.Position = _marker.position;
			bullet.Rotation = _marker.rotation;
			bullet.Behaviour.Select(ActionConsts.Idle);
		}

		public void Apply(IUnit target)
		{
			IEntity bullet = CreateBullet();
			if (bullet != null)
			{
				bullet.Behaviour.Context.SetGameObject(ActionConsts.TargetUnit, target.Owner);
				bullet.Behaviour.Select(ActionConsts.Idle);
			}
			else
			{
				target.BattleParams.Health.Value -= Config.Damage * (1 - target.BattleParams.Armor);
			}
		}

		public void Apply(Vector3 position)
		{
			IEntity bullet = CreateBullet();
			if (bullet == null)
				return;
			bullet.Behaviour.Context.SetVector3(ActionConsts.TargetPosition, position);
			bullet.Behaviour.Select(ActionConsts.Idle);
		}

		private IEntity CreateBullet()
		{
			if (string.IsNullOrEmpty(Config.PrefabId))
				return null;
			var bullet = _entityFactory.Spawn(Config.PrefabId);
			if (bullet == null)
				return null;
			bullet.Behaviour.Context.SetFloat(ActionConsts.Speed, Config.Speed);
			bullet.Behaviour.Context.SetFloat(ActionConsts.Damage, Config.Damage);
			return bullet;
		}

		public class Factory : IFactory<IUnitWeapon>
		{
			private DiContainer _container;
			public Factory(DiContainer container)
			{
				_container = container;
			}

			public IUnitWeapon Create()
			{
				return _container.Resolve<IUnitWeapon>();
			}
		}
	}
}
