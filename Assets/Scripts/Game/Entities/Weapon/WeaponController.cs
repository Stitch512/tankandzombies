﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Game.Configs;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Game.Entities.Weapon
{
	public class WeaponController: IWeaponController
	{
		public event Action<IUnitWeapon> Change;

		List<IUnitWeapon> _weapons = new List<IUnitWeapon>();

		private IFactory<IUnitWeapon> _weaponFactory;

		private IUnitWeapon _currentWeapon;

		public List<IUnitWeapon> Weapons => _weapons;

		public IUnitWeapon CurrentWeapon => _currentWeapon;

		private Vector2 _attackRange = new Vector2(float.MaxValue, float.MinValue);

		public Vector2 AttackRange => _attackRange;

		public WeaponController(IFactory<IUnitWeapon> weaponFactory)
		{
			_weaponFactory = weaponFactory;
		}

		public void SetConfig(List<IWeaponConfig> configs)
		{
			foreach (var config in configs)
			{
				IUnitWeapon weapon = _weaponFactory.Create();
				weapon.SetConfig(config);
				_weapons.Add(weapon);

				_attackRange.x = Mathf.Min(config.AttackRangeMin, _attackRange.x);
				_attackRange.y = Mathf.Max(config.AttackRangeMax, _attackRange.y);
			}
		}

		public void NextWeapon()
		{
			MoveWeapon(1);
		}

		public void PrevWeapon()
		{
			MoveWeapon(-1);
		}

		private void MoveWeapon(int dir)
		{
			if (_weapons.Count == 0)
				return;

			if (_currentWeapon == null)
			{
				_currentWeapon = _weapons[0];
			}
			else
			{

				int index = _weapons.IndexOf(_currentWeapon) + dir;
				if (index >= _weapons.Count)
					index = 0;
				if (index < 0)
					index = _weapons.Count - 1;
				_currentWeapon = _weapons[index];

			}

			Change?.Invoke(_currentWeapon);
		}

		public bool SwitchWeapon(float distance)
		{
			IUnitWeapon selectWeapon = null;
			foreach (var weapon in _weapons)
			{
				var config = weapon.Config;
				if (distance >= config.AttackRangeMin && distance <= config.AttackRangeMax)
				{
					selectWeapon = weapon;
					break;
				}
			}

			if (selectWeapon == null)
				return false;

			_currentWeapon = selectWeapon;
			Change?.Invoke(_currentWeapon);

			return true;
		}
	}
}
