﻿using Assets.Scripts.Game.Configs;
using UnityEngine;

namespace Assets.Scripts.Game.Entities.Weapon
{
	public interface IUnitWeapon
	{
		IWeaponConfig Config { get; }

		Transform Marker { get; }

		void SetConfig(IWeaponConfig config);

		void Apply();


		void Apply(IUnit target);

		void Apply(Vector3 position);

		void SetMarker(Transform marker);
	}
}
