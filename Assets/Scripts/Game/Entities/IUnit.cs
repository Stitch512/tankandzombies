﻿using Assets.Scripts.Entities;
using Assets.Scripts.Game.Entities.Weapon;

namespace Assets.Scripts.Game.Entities
{
	public interface IUnit: IEntity
	{
		IBattleParams BattleParams { get; }

		IWeaponController Weapon { get; }

	}
}
