﻿namespace Assets.Scripts.Game.Entities
{
	/// <summary>
	/// HHTC
	/// HH - type
	/// T - subtype
	/// C - color
	/// </summary>
	public enum EntityType: int
	{
		Undefined = 0x0000,

		Plaer = 0x1000,

		Enemy = 0x2000,
	}
}
