﻿using Assets.Scripts.Common.Behaviour.Actions;

namespace Assets.Scripts.Game.Entities
{
	public class BattleParams: IBattleParams
	{
		public BattleParams()
		{
			Health = new SharedValue<float>();
			Armor = new SharedValue<float>();
		}

		public SharedValue<float> Health { get; private set; }

		public SharedValue<float> Armor { get; private set; }
	}
}
