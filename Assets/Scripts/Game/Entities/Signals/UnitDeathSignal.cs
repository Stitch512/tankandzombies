﻿namespace Assets.Scripts.Game.Entities.Signals
{
	public class UnitDeathSignal
	{
		public IUnit Unit;
	}
}
