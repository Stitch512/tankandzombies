﻿namespace Assets.Scripts.Game.Entities.Signals
{
	public class UnitSpawnSignal
	{
		public IUnit Unit;
	}
}
