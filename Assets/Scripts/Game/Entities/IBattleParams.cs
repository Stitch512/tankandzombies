﻿using Assets.Scripts.Common.Behaviour.Actions;

namespace Assets.Scripts.Game.Entities
{
	public interface IBattleParams
	{
		SharedValue<float> Health { get; }

		SharedValue<float> Armor { get; }
	}
}
