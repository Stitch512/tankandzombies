﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Entities;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Game.Entities.Factory
{
	public abstract class EntityPoolBase<TParam, TContract>: IMemoryPool<TParam, TContract>, IFactory<TParam, TContract> where TContract : IEntity
	{
		Dictionary<TParam, Stack<TContract>> _inactiveItems;
		IFactory<TParam, TContract> _factory;

		private int _activeCount;

		[Inject]
		void Construct(IFactory<TParam, TContract> factory)
		{
			_factory = factory;

			_inactiveItems = new Dictionary<TParam, Stack<TContract>>();
		}

		public TContract Create(TParam id)
		{
			return Spawn(id);
		}

		public TContract Spawn(TParam id)
		{
			_activeCount++;

			TContract item;
			Stack<TContract> pool;
			if (!_inactiveItems.TryGetValue(id, out pool) || pool.Count == 0)
			{
				item = _factory.Create(id);
				OnSpawned(item, id, this);
				return item;
			}

			item = pool.Pop();
			OnSpawned(item, id, this);
			item.Owner.SetActive(true);
			return item;
		}

		protected virtual void OnSpawned(TContract item, TParam id, IMemoryPool pool)
		{

		}

		protected virtual void OnDespawn(TContract item)
		{

		}

		public void Despawn(TContract item)
		{
			_activeCount--;

			var config = item.Config;
			if (config == null)
			{
				GameObject.Destroy(item.Owner);
			}

			TParam key = GetKey(item);
			Stack<TContract> pool;
			if (!_inactiveItems.TryGetValue(key, out pool))
			{
				pool = new Stack<TContract>();
				_inactiveItems[key] = pool;
			}

			pool.Push(item);
			item.Owner.SetActive(false);
			OnDespawn(item);
		}

		protected abstract TParam GetKey(TContract item);

		public void Despawn(object obj)
		{
			Despawn((TContract)obj);
		}

		public int NumTotal => NumInactive + NumActive;

		public int NumActive => _activeCount;

		public int NumInactive => _inactiveItems.Sum(p => p.Value.Count);

		public Type ItemType => typeof(TContract);

		public void Clear()
		{
			foreach (var inactiveItems in _inactiveItems.Values)
			{
				while (inactiveItems.Count > 0)
				{
					var item = inactiveItems.Pop();
					GameObject.Destroy(item.Owner);
				}
			}
			_inactiveItems.Clear();
		}
		public void Resize(int desiredPoolSize)
		{
			throw new NotImplementedException();
		}

		public void ExpandBy(int numToAdd)
		{
			throw new NotImplementedException();
		}

		public void ShrinkBy(int numToRemove)
		{
			throw new NotImplementedException();
		}
	}
}
