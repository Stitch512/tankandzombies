﻿using Assets.Scripts.Entities;
using Assets.Scripts.Entities.Visual;
using Assets.Scripts.Game.Configs;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Game.Entities.Factory
{
	public class EntityFactory : PlaceholderFactory<string, IEntity>
	{
	}

	public class CustomEntityFactory: IFactory<string, IEntity>
	{
		private readonly IEntitiesConfigs _configs;

		private readonly DiContainer _container;

		public CustomEntityFactory(DiContainer container, IEntitiesConfigs configs)
		{
			_configs = configs;
			_container = container;
		}

		public IEntity Create(string id)
		{
			var config = _configs.GetConfig(id);
			if (config == null)
				return null;
			var visualConfig = config.VisualConfig;
			if (visualConfig == null)
				return null;

			var obj = _container.InstantiatePrefab(visualConfig.RootPrefab.gameObject);
			var entity = obj.GetComponent<Entity>();

			entity.SetConfig(config);

			var visuals = entity.GetComponentsInChildren<IEntityVisual>();
			foreach (var visual in visuals)
			{
				visual.Initialize(entity);
			}

			foreach (var visualsPrefab in visualConfig.VisualsPrefabs)
			{
				if (visualsPrefab == null)
					continue;

				var objVisual = _container.InstantiatePrefab(visualsPrefab.gameObject);
				var visual = objVisual.GetComponent<EntityVisual>();
				visual.transform.parent = entity.transform;
				visual.transform.localPosition = Vector3.zero;
				visual.Initialize(entity);
			}

			return entity;
		}
	}
}
