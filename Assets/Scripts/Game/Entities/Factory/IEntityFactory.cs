﻿using Assets.Scripts.Entities;

namespace Assets.Scripts.Game.Entities.Factory
{
	public interface IEntityFactory
	{
		IEntity Spawn(string id);

		void Despawn(IEntity item);
	}
}
