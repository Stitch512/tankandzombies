﻿using System;
using Assets.Scripts.Common.Behaviour;
using Assets.Scripts.Entities;
using Assets.Scripts.Game.Configs;
using Assets.Scripts.Game.Managers;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Game.Entities
{
	public class Entity: MonoBehaviour, IEntity
	{
		public int Id { get; }

		public event Action<Collision> CollisionEnter;

		public GameObject Owner => gameObject;

		public IEntityConfig Config { get; private set; }

		public T GetConfig<T>() where T: class, IEntityConfig
		{
			return Config as T;
		}

		public Vector3 Position
		{
			get { return transform.position; }
			set { transform.position = value; }
		}

		public Quaternion Rotation
		{
			get { return transform.rotation; }
			set { transform.rotation = value; }

		}

		private IBehaviourManager _behaviourManager;

		private IMemoryPool _pool;

		public IBehaviourManager Behaviour => _behaviourManager;

		[Inject]
		private void Construct(IGameScene scene, IBehaviourManager behaviourManager)
		{
			_behaviourManager = behaviourManager;
			_behaviourManager.Initialize(this);
		}

		public virtual void SetConfig(IEntityConfig config)
		{
			Config = config;
			_behaviourManager.Load(config.Behaviours);
		}

		protected virtual void Enable()
		{
			Animator animator = GetComponentInChildren<Animator>();
			animator?.Rebind();
		}

		protected virtual void Disable()
		{
			_behaviourManager.Clear();
		}

		public void OnSpawned(string id, IMemoryPool pool)
		{
			_pool = pool;
			Enable();
		}

		public void OnDespawned()
		{
			_pool = null;
			Disable();
		}

		public void Destroy()
		{
			if (_pool != null)
			{
				_pool.Despawn(this);
			}
			else
			{
				GameObject.Destroy(this);
			}
		}

		private void OnCollisionEnter(Collision collision)
		{
			CollisionEnter?.Invoke(collision);
		}

		private void Update()
		{
			_behaviourManager.Update();
		}

		public void Tick()
		{
			//_behaviourManager.Update();
		}
	}
}
