﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Entities;
using Assets.Scripts.Entities.Visual;
using Assets.Scripts.Game.Entities.Weapon;
using UnityEngine;

namespace Assets.Scripts.Game.Entities.Visual
{
	public class WaeponsVisual: MonoBehaviour, IEntityVisual
	{
		[Serializable]
		public class WaeponVisual
		{
			public string WeaponId;

			public GameObject Visual;
		}

		[SerializeField]
		private List<WaeponVisual> _waepons = new List<WaeponVisual>();

		public void Initialize(IEntity entity)
		{
			IUnit unit = entity as IUnit;
			if (unit == null)
				return;

			var unitWeapons = unit.Weapon.Weapons;

			foreach (var item in _waepons)
			{
				var weapon = unitWeapons.Find(w => w.Config.WeaponId == item.WeaponId);
				if (weapon == null)
					continue;
				Transform marker = item.Visual.transform.Find("marker");
				if (marker == null)
					continue;
				weapon.SetMarker(marker);
			}

			OnChangeWeapon(unit.Weapon.CurrentWeapon);

			unit.Weapon.Change += OnChangeWeapon;
		}

		private void OnChangeWeapon(IUnitWeapon weapon)
		{
			if (weapon == null)
				return;
			string weaponId = weapon.Config.WeaponId;
			foreach (var item in _waepons)
			{
				item.Visual.SetActive(item.WeaponId == weaponId);
			}
		}
	}
}
