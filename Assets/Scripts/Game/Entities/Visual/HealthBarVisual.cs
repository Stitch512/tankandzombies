﻿using Assets.Scripts.Entities;
using Assets.Scripts.Entities.Visual;
using Assets.Scripts.Game.Configs;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Game.Entities.Visual
{
	public class HealthBarVisual : EntityVisual
	{
		[SerializeField]
		private Slider _slider;

		private float _maxHealth;

		public override void Initialize(IEntity entity)
		{
			IUnit unit = entity as IUnit;
			if (unit == null)
				return;

			var config = unit.GetConfig<IUnitConfig>();
			_maxHealth = config.BattleConfig.Health;

			unit.BattleParams.Health.Change += OnHealthChange;

			UpdateHealth(unit.BattleParams.Health);
		}

		private void UpdateHealth(float value)
		{
			_slider.value = value / _maxHealth;
		}

		private void OnHealthChange(float value, float oldValue)
		{
			UpdateHealth(value);
		}
	}
}
