﻿using Assets.Scripts.Game.Actions;
using Assets.Scripts.Game.Configs;
using Assets.Scripts.Game.Entities.Signals;
using Assets.Scripts.Game.Entities.Weapon;
using Assets.Scripts.Utils;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Game.Entities
{
	public class Unit: Entity, IUnit
	{
		public IBattleParams BattleParams { get; private set; }

		public IWeaponController Weapon { get; private set; }

		private SignalBus _signalBus;


		[Inject]
		private void Construct(IBattleParams battleParams, IWeaponController weapon, SignalBus signalBus)
		{
			BattleParams = battleParams;
			Weapon = weapon;
			_signalBus = signalBus;
		}

		public override void SetConfig(IEntityConfig config)
		{
			base.SetConfig(config);

			IUnitConfig untiConfig = config as IUnitConfig;
			if (untiConfig == null)
				return;

			Weapon.SetConfig(untiConfig.BattleConfig.Weapons);
			Weapon.NextWeapon();
		}

		protected override void Enable()
		{
			base.Enable();

			var battleConfig = GetConfig<IUnitConfig>().BattleConfig;
			BattleParams.Health.Value = battleConfig.Health;
			BattleParams.Armor.Value = battleConfig.Armor;

			_signalBus.TryFire(new UnitSpawnSignal { Unit = this });
		}

		protected override void Disable()
		{
			base.Disable();

			_signalBus.TryFire(new UnitDeathSignal { Unit = this });
		}

		private void OnDrawGizmos()
		{
			Gizmos.color = Color.red;
			if (Behaviour != null)
			{
				var target = Behaviour.Context.GetGameObject(ActionConsts.TargetUnit);
				if (target != null)
				{
					Gizmos.DrawLine(this.transform.position, target.Value.transform.position);
				}
			}

			if (Weapon == null)
				return;

			foreach (var weapon in Weapon.Weapons)
			{
				Gizmos.color = Color.magenta;
				GizmosExtension.DrawCircle(this.transform.position, weapon.Config.AttackRangeMin);
				Gizmos.color = Color.red;
				GizmosExtension.DrawCircle(this.transform.position, weapon.Config.AttackRangeMax);
				//GizmosExtension.DrawTape(this.transform.position, weapon.WeaponConfig.AttackRangeMin, weapon.WeaponConfig.AttackRangeMax);

				var marker = weapon.Marker;
				if (marker != null)
				{
					Gizmos.DrawWireSphere(marker.position, 0.3f);
				}
			}

			Gizmos.color = Color.yellow;
			var config = GetConfig<IUnitConfig>();
			if (config != null)
			{
				GizmosExtension.DrawCircle(this.transform.position, config.BattleConfig.AgroRange);
			}
		}
	}
}
