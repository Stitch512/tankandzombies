﻿using System;
using Assets.Scripts.Common.Behaviour;
using Assets.Scripts.Game.Configs;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Entities
{
	public interface IEntity: ITickable, IPoolable<string, IMemoryPool>
	{
		int Id { get; }

		GameObject Owner { get; }

		event Action<Collision> CollisionEnter;

		IEntityConfig Config{ get; }

		T GetConfig<T>() where T :class, IEntityConfig;

		Vector3 Position { get; set; }

		Quaternion Rotation { get; set; }

		IBehaviourManager Behaviour { get; }

		void Destroy();
	}
}
