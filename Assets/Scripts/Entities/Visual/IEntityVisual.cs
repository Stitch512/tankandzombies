﻿namespace Assets.Scripts.Entities.Visual
{
	public interface IEntityVisual
	{
		void Initialize(IEntity entity);
	}
}
