﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Common.Config
{
#if UNITY_EDITOR

	public static class GUIUtils
	{
		private static Vector2 scrollPosition;
		public static void DrawList<T>(string label, IList<T> list) where T : IGUIDrawable, new()
		{
			//GUILayout.Space(10);
			GUILayout.BeginHorizontal();
			EditorGUILayout.LabelField(label);

			GUILayout.EndHorizontal();

			EditorGUILayout.Separator();

			scrollPosition = GUILayout.BeginScrollView(scrollPosition);

			for (int i = 0; i < list.Count; i++)
			{
				//GUILayout.Space(10);
				GUILayout.BeginHorizontal();
				list[i].Draw();

				//GUILayout.Space(10);
				if (GUILayout.Button("-"))
				{
					list.RemoveAt(i);
					i--;
				}

				GUILayout.EndHorizontal();
			}
			//GUILayout.EndScrollView();
			if (GUILayout.Button("Add", GUILayout.Height(15)))
			{
				list.Add(new T());
				/*if (list[list.Count - 1] is IScriptableListItem)
				{
					(list[list.Count - 1] as IScriptableListItem).ConfigId = label + (list.Count - 1).ToString();
				}*/
			}
			GUILayout.EndScrollView();
			//GUILayout.Space(10);
			return;
		}

		private static Vector2 scrollPosition1;
		public static void DrawObjectsList<T>(string label, IList<T> list) where T : Component
		{
			//GUILayout.Space(10);
			GUILayout.BeginHorizontal();
			EditorGUILayout.LabelField(label);

			GUILayout.EndHorizontal();

			EditorGUILayout.Separator();

			scrollPosition1 = GUILayout.BeginScrollView(scrollPosition1);

			for (int i = 0; i < list.Count; i++)
			{
				//GUILayout.Space(10);
				GUILayout.BeginHorizontal();
				//list[i].Draw();

				list[i] = EditorGUILayout.ObjectField("Visual", list[i], typeof(T), false) as T;

				//GUILayout.Space(10);
				if (GUILayout.Button("-"))
				{
					list.RemoveAt(i);
					i--;
				}

				GUILayout.EndHorizontal();
			}
			//GUILayout.EndScrollView();
			if (GUILayout.Button("Add", GUILayout.Height(15)))
			{
				list.Add(null);
				/*if (list[list.Count - 1] is IScriptableListItem)
				{
					(list[list.Count - 1] as IScriptableListItem).ConfigId = label + (list.Count - 1).ToString();
				}*/

			}
			GUILayout.EndScrollView();
			//GUILayout.Space(10);
			return;
		}

		//private static Vector2 scrollPosition;
		public static void DrawScriptableObjectsList<T>(string label, IList<T> list) where T : ScriptableObject
		{
			//GUILayout.Space(10);
			GUILayout.BeginHorizontal();
			EditorGUILayout.LabelField(label);
			GUILayout.EndHorizontal();


			EditorGUILayout.Separator();

			scrollPosition = GUILayout.BeginScrollView(scrollPosition);

			for (int i = 0; i < list.Count; i++)
			{
				//GUILayout.Space(10);
				GUILayout.BeginHorizontal();

				list[i] = EditorGUILayout.ObjectField("Asset", list[i], typeof(T), false) as T;

				//GUILayout.Space(10);
				if (GUILayout.Button("-"))
				{
					list.RemoveAt(i);
					i--;
				}

				GUILayout.EndHorizontal();
			}
			//GUILayout.EndScrollView();
			if (GUILayout.Button("Add", GUILayout.Height(15)))
			{
				list.Add(default(T));
				//if (list[list.Count - 1] is IScriptableListItem)
				//{
				//	(list[list.Count - 1] as IScriptableListItem).ConfigId = label + (list.Count - 1).ToString();
				//}

			}
			GUILayout.EndScrollView();
			//GUILayout.Space(10);
			return;
		}
	}

#endif
}
