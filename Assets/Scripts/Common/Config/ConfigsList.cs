﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Common.Config
{
	[Serializable]
	public class ConfigsList<T>: ScriptableObjectConfig, IList<T> where T: ScriptableObjectConfig
	{
		[SerializeField]
		protected List<T> _childs = new List<T>();

		public IEnumerator<T> GetEnumerator()
		{
			return _childs.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public void Add(T item)
		{
			_childs.Add(item);
		}

		public void Clear()
		{
			_childs.Clear();
		}

		public bool Contains(T item)
		{
			return _childs.Contains(item);
		}

		public void CopyTo(T[] array, int arrayIndex)
		{
			_childs.CopyTo(array, arrayIndex);
		}

		public bool Remove(T item)
		{
			return _childs.Remove(item);
		}

		public int Count => _childs.Count;

		public bool IsReadOnly => false;

		public int IndexOf(T item)
		{
			return _childs.IndexOf(item);
		}

		public void Insert(int index, T item)
		{
			_childs.Insert(index, item);
		}

		public void RemoveAt(int index)
		{
			_childs.RemoveAt(index);
		}

		public T this[int index]
		{
			get { return _childs[index]; }
			set { _childs[index] = value; }
		}
	}
}
