﻿using System;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Common.Config
{
	[Serializable]
	public class ScriptableObjectConfig: ScriptableObject, IGUIDrawable
	{
		public string ConfigId
		{
			get
			{
#if UNITY_EDITOR
				if (string.IsNullOrEmpty(newName))
					newName = name;
				return newName;

#else
                return name;
#endif
			}
			set
			{

#if UNITY_EDITOR
				if (value != "")
					newName = value;
#endif
			}
		}

#if UNITY_EDITOR

		string newName = String.Empty;

		Vector2 scrolPos;
		public void Draw()
		{
			scrolPos = EditorGUILayout.BeginScrollView(scrolPos);

			OnDraw();

			EditorGUILayout.EndScrollView();
		}

		protected virtual void OnDraw()
		{
		}

		public virtual void Save()
		{
			if (name != newName && newName != "")
			{
				AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(this), newName);
				AssetDatabase.SaveAssets();
			}

			EditorUtility.SetDirty(this);
		}
#endif

	}
}
