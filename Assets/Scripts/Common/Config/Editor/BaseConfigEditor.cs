﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Common.Config
{
	public class BaseConfigEditor<T>: EditorWindow where T : ScriptableObjectConfig
	{
		private ConfigsList<T> _configs;

		private static Vector2 scrollPosition;

		private Dictionary<int, bool> _toggles = new Dictionary<int, bool>();

		public void OnGUI()
		{
			_configs = EditorGUILayout.ObjectField("Asset", _configs, typeof(ConfigsList<T>), false) as ConfigsList<T>;

			if (_configs == null)
				return;

			//GUIUtils.DrawScriptableObjectsList("Configs", _configs);

			//GUILayout.Space(10);
			//GUILayout.BeginHorizontal();
			//EditorGUILayout.LabelField(label);
			//GUILayout.EndHorizontal();


			EditorGUILayout.Separator();

			scrollPosition = GUILayout.BeginScrollView(scrollPosition);

			DrawUILine(Color.black);

			for (int i = 0; i < _configs.Count; i++)
			{
				//GUILayout.Space(10);
				GUILayout.BeginHorizontal();

				if (!_toggles.ContainsKey(i))
					_toggles[i] = false;
				_toggles[i] = GUILayout.Toggle(_toggles[i], _toggles[i] ? "" : _configs[i]?.ConfigId);

				if (!_toggles[i])
				{
					GUILayout.EndVertical();
					continue;
				}

				GUILayout.BeginVertical();

				_configs[i] = EditorGUILayout.ObjectField("Asset", _configs[i], typeof(T), false) as T;

				if (_configs[i] != null)
					_configs[i].Draw();

				GUILayout.EndVertical();

				//GUILayout.Space(10);
				if (GUILayout.Button("-"))
				{
					_configs.RemoveAt(i);
					i--;
				}

				GUILayout.EndHorizontal();

				DrawUILine(Color.black);
			}
			//GUILayout.EndScrollView();
			if (GUILayout.Button("Add", GUILayout.Height(15)))
			{
				_configs.Add(default(T));
				//if (list[list.Count - 1] is IScriptableListItem)
				//{
				//	(list[list.Count - 1] as IScriptableListItem).ConfigId = label + (list.Count - 1).ToString();
				//}

			}
			GUILayout.EndScrollView();
			//GUILayout.Space(10);

			if (GUILayout.Button("Save"))
			{
				foreach (var config in _configs)
				{
					EditorUtility.SetDirty(config);
				}

				EditorUtility.SetDirty(_configs);
				AssetDatabase.SaveAssets();
				AssetDatabase.Refresh();
			}
		}

		public static void DrawUILine(Color color, int thickness = 2, int padding = 10)
		{
			Rect r = EditorGUILayout.GetControlRect(GUILayout.Height(padding + thickness));
			r.height = thickness;
			r.y += padding / 2;
			r.x -= 2;
			r.width += 6;
			EditorGUI.DrawRect(r, color);
		}

		/*private string _typeName;

		private T _viewElement = null;

		void OnEnable()
		{
			_typeName = typeof(T).Name;
		}

		public T viewElement
		{
			get
			{
				if (_viewElement == null)
				{
					viewIndex = 0;
				}
				return _viewElement;
			}
			set
			{
				_viewElement = value;
				viewIndex = _configs.IndexOf(value as T);
			}
		}
		public int viewIndex
		{
			get
			{
				return _configs.IndexOf(viewElement as T);
			}
			set
			{
				if (_configs.Count > 0)
				{
					_viewElement = _configs[Mathf.Clamp(value, 0, _configs.Count - 1)];
				}
			}
		}

		public void OnGUI()
		{
			_configs = EditorGUILayout.ObjectField("Asset", _configs, typeof(ConfigsList<T>), false) as ConfigsList<T>;

			if (_configs == null)
				return;

			GUILayout.BeginHorizontal();
			DrawList();
			GUILayout.BeginVertical();

			GUILayout.BeginHorizontal();
			if (_configs != null)
			{
				if (GUILayout.Button("Add Item", GUILayout.ExpandWidth(false)))
				{
					AddItem();
				}

				GUILayout.Space(10);

				if (GUILayout.Button("Prev", GUILayout.ExpandWidth(false)))
				{
					viewIndex--;
					GUI.FocusControl("clearFocus");
				}
				GUILayout.Space(5);
				if (GUILayout.Button("Next", GUILayout.ExpandWidth(false)))
				{
					viewIndex++;
					GUI.FocusControl("clearFocus");
				}
				if (_configs.Count > 0 && GUILayout.Button("Delete Item", GUILayout.ExpandWidth(false)))
				{
					DeleteItem(viewIndex);
				}
				GUI.backgroundColor = Color.white;
				if (_configs.Count == 0)
				{
					return;
				}
				GUILayout.Space(5);
				GUILayout.FlexibleSpace();
			}

			GUILayout.EndHorizontal();
			GUILayout.Space(10);
			if (_configs.Count > 0)
			{
				_configs[viewIndex].Draw();
				if (GUI.changed)
					_configs[viewIndex].Save();
			}
			GUILayout.EndVertical();
			GUILayout.EndHorizontal();
		}

		float itemInRaw = 1;
		private Vector2 scrollPos;

		private void DrawList()
		{
			if (_configs == null)
				return;

			GUILayout.BeginVertical();
			int width = 130;
			itemInRaw = Mathf.Clamp(width / 150, 1, int.MaxValue);
			//if (list == null)
			//{
			//	newSelectedItem = null;
			//}
			//newSelectedItem = selectedItem;

			scrollPos = GUILayout.BeginScrollView(scrollPos, false, true, GUILayout.Width(width));

			int selected = -1;
			List<GUIContent> content = new List<GUIContent>();

			for (int i = _configs.Count - 1; i >= 0; i--)
			{
				if (_configs[i] == null)
					_configs.RemoveAt(i);
			}

			var selectedItem = viewElement;
			for (int i = 0; i < _configs.Count; i++)
			{
				content.Add(new GUIContent(_configs[i].name));
				if (selectedItem != null && _configs[i].ConfigId == selectedItem.ConfigId)
					selected = content.Count - 1;
			}

			selected = GUILayout.SelectionGrid(selected, content.ToArray(), Mathf.RoundToInt(itemInRaw), GUILayout.MaxWidth(150 * itemInRaw));

			if (selected < _configs.Count && selected >= 0)
			{
				var newItem = _configs[selected];

				if (newItem != viewElement)
				{
					viewElement = newItem;
					GUI.FocusControl(null);
				}
			}

			GUILayout.EndScrollView();
			GUILayout.EndVertical();
		}

		void AddItem()
		{
			viewElement = AddConfig();
		}

		void DeleteItem(int index)
		{
			DeleteConfig(index);
			viewIndex--;
		}

		private static string configPath
		{
			get
			{
				return "Assets/Test/Resources/Configs";
			}
		}
		private string path
		{
			get
			{
				return configPath + "/" + _typeName;
			}
		}

		public T AddConfig(string id = "")
		{
			T instance = ScriptableObject.CreateInstance<T>();
			instance.name = id == "" ? $"{_typeName}{_configs.Count}" : id;
			_configs.Add(instance);

			var assetFullPath = path + "/" + instance.name + ".asset";
			AssetDatabase.CreateAsset(instance, assetFullPath);
			AssetDatabase.SaveAssets();

			return instance;
		}

		public void DeleteConfig(int index)
		{
			if (index < 0 || index >= _configs.Count)
				return;
			AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(_configs[index]));
			_configs.RemoveAt(index);
		}

#if !UNITY_EDITOR
		private void LoadConfigs()
		{
			_childs = new List<T>(Resources.LoadAll<T>(AssetDatabaseUtil.GetResourceFolderPath(path)));
		}
#else
		public void LoadConfigs()
		{
			if (!AssetDatabase.IsValidFolder(configPath))
			{
				AssetDatabase.CreateFolder("Assets/Test/Resources", "Configs");
			}
			if (!AssetDatabase.IsValidFolder(path))
			{
				AssetDatabase.CreateFolder(configPath, _typeName);
			}

			string[] configFiles = Directory.GetFiles(Application.dataPath + path.Replace("Assets", ""), "*.asset", SearchOption.AllDirectories);
			foreach (string configFile in configFiles)
			{

				string assetPath = path + configFile.Replace(Application.dataPath + path.Replace("Assets", ""), "").Replace('\\', '/');
				T source = AssetDatabase.LoadAssetAtPath(assetPath, typeof(T)) as T;
				if (source != null)
				{
					_configs.Add(source);
				}
				else
				{
					Debug.LogError($"Invalid load '{assetPath}'");
				}
			}
		}
#endif

		private void OnDestroy()
		{
			//_configs.SaveAllNewItem();
			viewElement = null;
			_configs = null;
		}*/
	}
	}
