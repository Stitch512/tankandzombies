﻿using Assets.Scripts.Game.Configs.Data;
using UnityEditor;

namespace Assets.Scripts.Common.Config.Editor
{
	public class EntityConfigEditor : BaseConfigEditor<EntityConfig>
	{
		[MenuItem("Game/Configs/Config")]
		public static void InitEntityConfigEditorWindow()
		{
			EditorWindow.GetWindow(typeof(EntityConfigEditor));
		}
	}
}
