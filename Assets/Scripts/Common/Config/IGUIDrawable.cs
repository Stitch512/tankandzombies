﻿namespace Assets.Scripts.Common.Config
{
	public interface IGUIDrawable
	{
#if UNITY_EDITOR
		void Draw();
#endif
	}
}
