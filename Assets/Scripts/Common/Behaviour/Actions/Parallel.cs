﻿namespace Assets.Scripts.Common.Behaviour.Actions
{
	[GameAction]
	public sealed class Parallel : BaseParallel
	{
		public Parallel() : base(ActionStatus.Failure, ActionStatus.Success)
		{
		}

		public Parallel(params IGameAction[] actions) : base(ActionStatus.Failure, ActionStatus.Success, actions)
		{
		}
	}

	[GameAction]
	public sealed class ParallelSelector : BaseParallel
	{
		public ParallelSelector() : base(ActionStatus.Success, ActionStatus.Failure)
		{
		}

		public ParallelSelector(params IGameAction[] actions) : base(ActionStatus.Success, ActionStatus.Failure, actions)
		{
		}
	}

	public abstract class BaseParallel : Composite
	{
		private readonly ActionStatus _breakStatus;

		private readonly ActionStatus _returnStatus;

		protected BaseParallel(ActionStatus breakStatus, ActionStatus returnStatus)
		{
			_breakStatus = breakStatus;
			_returnStatus = returnStatus;
		}

		protected BaseParallel(ActionStatus breakStatus, ActionStatus returnStatus, params IGameAction[] actions) : base(actions)
		{
			_breakStatus = breakStatus;
			_returnStatus = returnStatus;
		}

		protected override void OnStart()
		{
			if (Count == 0)
			{
				End(_returnStatus);
				return;
			}

			foreach (var cmd in this)
			{
				cmd.Start();
			}
		}

		protected override void OnUpdate()
		{
			base.OnUpdate();

			var completedCount = 0;
			foreach (var child in this)
			{
				if (child.Status == _breakStatus)
				{
					End(_breakStatus);
					return;
				}
				if (child.Status != ActionStatus.Running)
					completedCount++;
			}
			if (completedCount == Count)
				End(_returnStatus);
		}

		protected override void OnEnd()
		{
			foreach (var action in this)
			{
				action.Stop();
			}
		}
	}
}
