﻿namespace Assets.Scripts.Common.Behaviour.Actions
{
	public enum ActionStatus
	{
		Inactive = 0,
		Failure = 1,
		Success = 2,
		Running = 3
	}

	public interface IGameAction
	{
		ActionStatus Status { get; }

		void Start();

		void Stop();

		void Update();
	}
}
