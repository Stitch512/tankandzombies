﻿namespace Assets.Scripts.Common.Behaviour.Actions
{
	public abstract class GameAction: IGameAction
	{
		private ActionStatus _status = ActionStatus.Inactive;

		public ActionStatus Status => _status;

		public void Start()
		{
			if (_status == ActionStatus.Running)
				return;
			_status = ActionStatus.Running;
			OnStart();
		}

		public void Stop()
		{
			End(ActionStatus.Inactive);
		}

		protected void End(ActionStatus status)
		{
			if (_status != ActionStatus.Running)
				return;
			_status = status;
			OnEnd();
		}

		public void Update()
		{
			OnUpdate();
		}

		protected virtual void OnStart()
		{

		}

		protected virtual void OnEnd()
		{

		}

		protected virtual void OnUpdate()
		{

		}
	}
}
