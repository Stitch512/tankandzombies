﻿using System.Linq;

namespace Assets.Scripts.Common.Behaviour.Actions
{
	[GameAction]
	public class Success : OverrideResult
	{
		public Success(): base(ActionStatus.Success)
		{
		}
	}

	[GameAction]
	public class Failure : OverrideResult
	{
		public Failure() : base(ActionStatus.Failure)
		{
		}
	}

	public abstract class OverrideResult: Composite
	{
		private ActionStatus _returnStatus;

		protected OverrideResult(ActionStatus returnStatus)
		{
			_returnStatus = returnStatus;
		}

		protected override void OnStart()
		{
			if (Count == 0)
			{
				End(_returnStatus);
				return;
			}

			foreach (var cmd in this)
			{
				cmd.Start();
			}
		}

		protected override void OnUpdate()
		{
			base.OnUpdate();

			if (this.Any(c => c.Status == ActionStatus.Running))
				return;

			End(_returnStatus);
		}
	}
}
