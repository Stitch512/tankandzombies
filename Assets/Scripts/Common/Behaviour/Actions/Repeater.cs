﻿using System.Linq;

namespace Assets.Scripts.Common.Behaviour.Actions
{
	[GameAction]
	public class Repeater: Composite
	{
		protected override void OnStart()
		{
			if (Count == 0)
			{
				End(ActionStatus.Success);
				return;
			}

			foreach (var cmd in this)
			{
				cmd.Start();
			}
		}

		protected override void OnUpdate()
		{
			base.OnUpdate();

			if (this.Any(c => c.Status == ActionStatus.Running))
				return;

			foreach (var child in this)
			{
				child.Start();
			}
		}
	}
}
