﻿namespace Assets.Scripts.Common.Behaviour.Actions
{
	public abstract class EntityAction: GameAction
	{
		public ActionContext Context { get; set; }

		protected EntityAction()
		{
		}
	}
}
