﻿using System.Collections.Generic;

namespace Assets.Scripts.Common.Behaviour.Actions
{
	[GameAction]
	public sealed class Sequence : BaseSequence
	{
		public Sequence() : base(ActionStatus.Failure, ActionStatus.Success)
		{
		}

		public Sequence(params IGameAction[] actions) : base(ActionStatus.Failure, ActionStatus.Success, actions)
		{
		}
	}

	[GameAction]
	public sealed class Selector : BaseSequence
	{
		public Selector() : base(ActionStatus.Success, ActionStatus.Failure)
		{
		}

		public Selector(params IGameAction[] actions) : base(ActionStatus.Success, ActionStatus.Failure, actions)
		{
		}
	}

	public abstract class BaseSequence : Composite
	{
		private IEnumerator<IGameAction> _enumerator;

		private readonly ActionStatus _breakStatus;

		private readonly ActionStatus _returnStatus;

		protected BaseSequence(ActionStatus breakStatus, ActionStatus returnStatus)
		{
			_breakStatus = breakStatus;
			_returnStatus = returnStatus;
		}

		protected BaseSequence(ActionStatus breakStatus, ActionStatus returnStatus, params IGameAction[] actions) : base(actions)
		{
			_breakStatus = breakStatus;
			_returnStatus = returnStatus;
		}

		protected override void OnStart()
		{
			_enumerator?.Dispose();
			_enumerator = GetEnumerator();
			_enumerator.Reset();
			if (!_enumerator.MoveNext())
			{
				Stop();
				return;
			}

			_enumerator.Current?.Start();
		}

		protected override void OnUpdate()
		{
			base.OnUpdate();

			if (_enumerator == null)
				return;

			var current = _enumerator.Current;
			if (current == null)
			{
				Stop();
				return;
			}

			if (current.Status == ActionStatus.Running)
				return;

			if (current.Status == _breakStatus)
			{
				End(_breakStatus);
				return;
			}

			if (!_enumerator.MoveNext())
			{
				End(_returnStatus);
				return;
			}

			_enumerator.Current?.Start();
		}

		protected override void OnEnd()
		{
			_enumerator.Dispose();
			_enumerator = null;
			foreach (var child in this)
			{
				child.Stop();
			}
		}
	}
}
