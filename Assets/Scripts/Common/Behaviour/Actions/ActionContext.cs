﻿using System.Collections.Generic;
using Assets.Scripts.Entities;
using UnityEngine;

namespace Assets.Scripts.Common.Behaviour.Actions
{
	public class ActionContext
	{
		private readonly Dictionary<string, SharedValue<int>> _intValues = new Dictionary<string, SharedValue<int>>();

		private readonly Dictionary<string, SharedValue<float>> _floatValues = new Dictionary<string, SharedValue<float>>();

		private readonly Dictionary<string, SharedValue<bool>> _boolValues = new Dictionary<string, SharedValue<bool>>();

		private readonly Dictionary<string, SharedValue<Vector3>> _vector3Values = new Dictionary<string, SharedValue<Vector3>>();

		private readonly Dictionary<string, SharedValue<GameObject>> _gameObjectValues = new Dictionary<string, SharedValue<GameObject>>();

		public IEntity Owner { get; set; }

		private void SetValue<T>(string key, T value, Dictionary<string, SharedValue<T>> values)
		{
			SharedValue<T> sharedValue;
			if (!values.TryGetValue(key, out sharedValue))
			{
				sharedValue = new SharedValue<T>(value);
				values[key] = sharedValue;
			}
			else
			{
				sharedValue.Value = value;
			}
		}

		private SharedValue<T> GetValue<T>(string key, Dictionary<string, SharedValue<T>> values)
		{
			SharedValue<T> sharedValue;
			if (!values.TryGetValue(key, out sharedValue))
				return null;
			//{
			//	sharedValue = new SharedValue<T>(default(T));
			//	values[key] = sharedValue;
			//}
			return sharedValue;
		}

		public void SetGameObject(string key, GameObject value)
		{
			SetValue(key, value, _gameObjectValues);
		}

		public SharedValue<GameObject> GetGameObject(string key)
		{
			return GetValue(key, _gameObjectValues);
		}

		public void RemoveGameObject(string key)
		{
			_gameObjectValues.Remove(key);
		}

		public void SetInt(string key, int value)
		{
			SetValue(key, value, _intValues);
		}

		public SharedValue<int> GetInt(string key)
		{
			return GetValue(key, _intValues);
		}

		public void RemoveInt(string key)
		{
			_intValues.Remove(key);
		}

		public void SetFloat(string key, float value)
		{
			SetValue(key, value, _floatValues);
		}

		public SharedValue<float> GetFloat(string key)
		{
			return GetValue(key, _floatValues);
		}

		public void RemoveFloat(string key)
		{
			_floatValues.Remove(key);
		}

		public void SetBool(string key, bool value)
		{
			SetValue(key, value, _boolValues);
		}

		public SharedValue<bool> GetBool(string key)
		{
			return GetValue(key, _boolValues);
		}

		public void RemoveBool(string key)
		{
			_boolValues.Remove(key);
		}

		public void SetVector3(string key, Vector3 value)
		{
			SetValue(key, value, _vector3Values);
		}

		public SharedValue<Vector3> GetVector3(string key)
		{
			return GetValue(key, _vector3Values);
		}

		public void RemoveVector3(string key)
		{
			_vector3Values.Remove(key);
		}

		public void Clear()
		{
			_intValues.Clear();
			_floatValues.Clear();
			_boolValues.Clear();
			_vector3Values.Clear();
			_gameObjectValues.Clear();
		}
	}
}
