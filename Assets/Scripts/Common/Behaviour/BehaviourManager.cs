﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Common.Behaviour.Actions;
using Assets.Scripts.Common.Behaviour.Config;
using Assets.Scripts.Entities;
using Zenject;

namespace Assets.Scripts.Common.Behaviour
{
	public class BehaviourManager: IBehaviourManager
	{
		private readonly ActionContext _context = new ActionContext();

		public ActionContext Context => _context;

		private readonly Dictionary<string, IGameAction> _actions = new Dictionary<string, IGameAction>();

		private IGameAction _current;

		public IGameAction Current => _current;

		private DiContainer _container;

		public BehaviourManager(DiContainer container)
		{
			_container = container;
		}

		public void Initialize(IEntity owner)
		{
			_context.Owner = owner;
		}

		public void Load(List<BehaviourConfig> configs)
		{
			foreach (var config in configs)
			{
				LoadConfig(config);
			}
		}

		private void LoadConfig(BehaviourConfig config)
		{
			if (config == null)
				return;
			IGameAction action = CreateAction(config.Action);
			if (action == null)
				return;

			Register(config.Id, action);
		}

		private IGameAction CreateAction(ActionConfig config)
		{
			if (config == null || string.IsNullOrEmpty(config.TypeName))
				return null;
			Type type = Type.GetType(config.TypeName);
			IGameAction action = _container.Instantiate(type) as IGameAction;

			Composite composite = action as Composite;
			if (composite != null)
			{
				foreach (var childConfig in config.Childs)
				{
					var childAction = CreateAction(childConfig);
					if (childAction != null)
						composite.Add(childAction);
				}
			}

			return action;
		}

		public void Register(string key, IGameAction action)
		{
			_actions[key] = action;
			SetContext(action);
		}

		public void Select(string key)
		{
			IGameAction action;
			if (!_actions.TryGetValue(key, out action) || action == null)
				return;
			if (_current == action)
				return;

			_current?.Stop();
			_current = action;
			_current.Start();
		}


		private void SetContext(IGameAction owner)
		{
			var task = owner as EntityAction;
			if (task != null)
				task.Context = _context;
			var composite = owner as Composite;
			if (composite != null)
			{
				foreach (var child in composite)
				{
					SetContext(child);
				}
			}
		}

		public void Update()
		{
			_current?.Update();
		}

		public void Clear()
		{
			_context.Clear();
			_current?.Stop();
			_current = null;
		}
	}
}
