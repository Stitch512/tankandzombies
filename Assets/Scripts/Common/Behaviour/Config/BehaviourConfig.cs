﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Common.Config;
using UnityEngine;

namespace Assets.Scripts.Common.Behaviour.Config
{
	[Serializable]
	public class ActionConfig
	{
		[SerializeField]
		public string TypeName;

		[SerializeField]
		public List<ActionConfig> Childs = new List<ActionConfig>();
	}

	[CreateAssetMenu(fileName = "BehaviourConfig", menuName = "Game/Test/BehaviourConfig", order = 1)]
	[Serializable]
	public class BehaviourConfig: ScriptableObjectConfig
	{
		[SerializeField]
		public string Id;

		[SerializeField]
		public ActionConfig Action = new ActionConfig();

		/*
#if UNITY_EDITOR
		Vector2 scrolPos;
		public override void Draw()
		{
			scrolPos = EditorGUILayout.BeginScrollView(scrolPos);

			base.Draw();

			EditorGUILayout.Separator();
			_configs = EditorGUILayout.ObjectField("Asset", _configs, typeof(ConfigsList<T>), false) as ConfigsList<T>;

			EditorGUILayout.Separator();

			_battleConfig.Draw();
			EditorGUILayout.Separator();

			EditorGUILayout.Separator();

			EditorGUILayout.EndScrollView();
		}
#endif
		*/
	}
}
