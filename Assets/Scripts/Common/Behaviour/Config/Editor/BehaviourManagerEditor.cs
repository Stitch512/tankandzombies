﻿using Assets.Scripts.Common.Behaviour.Actions;
using Assets.Scripts.Game.Entities;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Common.Behaviour.Config.Editor
{
	public class BehaviourManagerEditor : EditorWindow
	{
		[MenuItem("Game/Behaviour Debugger")]
		public static void ShowWindow()
		{
			BehaviourManagerEditor window =
				(BehaviourManagerEditor)EditorWindow.GetWindow(typeof(BehaviourManagerEditor), false, "Behaviour Debugger");
			window.Show();
		}

		public static Transform selectedObject;

		public static IBehaviourManager logicBehaviour;

		private void OnEnable()
		{
			EditorApplication.update = UpdateSelection;
			UpdateSelection();
		}

		void OnDisable()
		{
			EditorApplication.update = null;
		}

		void UpdateSelection()
		{
			selectedObject = UnityEditor.Selection.activeTransform;
			if (selectedObject != null)
			{
				var entity = selectedObject.GetComponentInChildren<Entity>();
				logicBehaviour = entity?.Behaviour;
			}
			else
			{
				logicBehaviour = null;
			}

			Repaint();
		}

		public void OnSelectionChange()
		{
			UpdateSelection();
		}

		public void OnGUI()
		{
			if (logicBehaviour != null)
				DrawBehaviourTree(logicBehaviour);

			Repaint();
		}

		private void DrawBehaviourTree(IBehaviourManager debugger)
		{
			EditorGUILayout.BeginVertical();
			{
				if (debugger.Current != null)
				{
					DrawNodeTree(debugger.Current, 0, "");
				}
			}
			EditorGUILayout.EndVertical();
		}

		private static GUIContent SomeBox = new GUIContent("SomeBox", "It is just a box");

		private void DrawNodeTree(IGameAction node, int depth, string offset)
		{
			if (node == null)
				return;

			GUI.color = (node.Status == ActionStatus.Running) ? new Color(1f, 1f, 1f, 1f) : new Color(1f, 1f, 1f, 0.3f);

			var guicolor_backup = GUI.backgroundColor;
			GUI.backgroundColor = (node.Status == ActionStatus.Running) ? new Color(1f, 1f, 1f, 1f) : new Color(1f, 1f, 1f, 0.3f);
			GUILayout.Box(new GUIContent(offset + node.GetType().Name, ""));
			GUI.backgroundColor = guicolor_backup;

			var composit = node as Composite;
			if (composit != null)
			{
				foreach (var child in composit)
				{
					DrawNodeTree(child, depth + 1, offset + "   ");
				}
			}
		}

		private void DrawSpacing()
		{
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.Space();
			EditorGUILayout.EndHorizontal();
		}
	}
}
