﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Assets.Scripts.Common.Behaviour.Actions;
using ModestTree;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Common.Behaviour.Config.Editor
{
	[CustomEditor(typeof(BehaviourConfig))]
	public class BehaviourConfigEditor: UnityEditor.Editor
	{
		private Dictionary<string, Type> _types = new Dictionary<string, Type>();

		private string[] _names = new string[0];

		private void OnEnable()
		{
			_types.Clear();
			var classes = new List<Type>(Assembly.GetAssembly(typeof(GameAction))
				.GetTypes()
				.Where(t => t.IsSubclassOf(typeof(GameAction))).ToArray()).ToArray();
			foreach (var cClass in classes)
			{
				var attr = cClass.GetCustomAttribute<GameActionAttribute>();
				if (attr == null)
					continue;
				_types[cClass.FullName] = cClass;
			}

			_names = _types.Keys.ToArray();
		}

		public override void OnInspectorGUI()
		{
			//DrawDefaultInspector();
			//return;

			BehaviourConfig config = target as BehaviourConfig;
			if (config == null)
				return;

			config.Id = EditorGUILayout.TextField("Id", config.Id);

			bool add = false;
			bool rem = false;
			config.Action = DrawAction(config.Action, 0, null, ref add, ref rem);

			serializedObject.ApplyModifiedProperties();

			if (GUILayout.Button("Save"))
			{
				EditorUtility.SetDirty(config);
				AssetDatabase.SaveAssets();
				AssetDatabase.Refresh();
			}
		}

		private ActionConfig DrawAction(ActionConfig action, int offset, List<ActionConfig> parent, ref bool add, ref bool rem)
		{
			EditorGUILayout.BeginHorizontal();

			GUILayout.Space(offset * 30);

			Type type = null;
			int index = action != null ? _names.IndexOf(action.TypeName) : -1;
			index = EditorGUILayout.Popup(index, _names);
			if (action == null)
				action = new ActionConfig();
			if (index >= 0 && index < _names.Length && _types.TryGetValue(_names[index], out type))
			{
				action.TypeName = type.FullName;
			}

			if (parent != null)
			{
				if (GUILayout.Button("+"))
				{
					add = true;
				}

				if (GUILayout.Button("-"))
				{
					rem = true;
				}
			}

			EditorGUILayout.EndHorizontal();

			if (type != null)
			{
				if (type.IsSubclassOf(typeof(Composite)))
				{
					var childs = action.Childs;
					for (int i = 0; i < childs.Count; i++)
					{
						bool isAdd = false;
						bool isRem = false;
						childs[i] = DrawAction(childs[i], offset + 1, action.Childs, ref isAdd, ref isRem);
						if (isAdd)
						{
							childs.Insert(i + 1, new ActionConfig());
						}

						if (isRem)
						{
							childs.RemoveAt(i);
						}
					}

					if (childs.Count == 0)
					{
						EditorGUILayout.BeginHorizontal();

						GUILayout.Space(offset * 30);

						if (GUILayout.Button("+"))
						{
							action.Childs.Add(new ActionConfig());
						}

						EditorGUILayout.EndHorizontal();
					}
				}
			}

			return action;
		}
	}
}
