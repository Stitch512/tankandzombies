﻿using System.Collections.Generic;
using Assets.Scripts.Common.Behaviour.Actions;
using Assets.Scripts.Common.Behaviour.Config;
using Assets.Scripts.Entities;

namespace Assets.Scripts.Common.Behaviour
{
	public interface IBehaviourManager
	{
		ActionContext Context { get; }

		IGameAction Current { get; }

		void Initialize(IEntity owner);

		void Load(List<BehaviourConfig> configs);

		void Register(string key, IGameAction action);

		void Select(string key);

		void Update();

		void Clear();
	}
}
