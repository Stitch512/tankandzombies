﻿using Assets.Scripts.Entities;
using UnityEngine;

namespace Assets.Scripts.Utils
{
	public static class MathUtils
	{
		public static bool IsDestinated(this IEntity entity, Vector3 target, float distance)
		{
			var pos = entity.Position;
			float dx = pos.x - target.x;
			float dz = pos.z - target.z;
			return Mathf.Sqrt(dx * dx + dz * dz) < distance;
		}

		public static float Distance2d(Vector3 p1, Vector3 p2)
		{
			float dx = p1.x - p2.x;
			float dz = p1.z - p2.z;
			return Mathf.Sqrt(dx * dx + dz * dz);
		}
	}
}
